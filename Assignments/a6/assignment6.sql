--Drops any previous tables/indices
DROP TABLE a6_books CASCADE CONSTRAINTS;
DROP TABLE a6_countries CASCADE CONSTRAINTS;
DROP TABLE a6_authors CASCADE CONSTRAINTS;
DROP TABLE a6_authors_books CASCADE CONSTRAINTS;

DROP INDEX a6_book_index;
DROP INDEX a6_author_index;

--Create tables
CREATE TABLE a6_books (
    isbn VARCHAR2(13) PRIMARY KEY,
    title VARCHAR2(50) NOT NULL,
    category VARCHAR2(50),
    price NUMBER(5,2) CHECK (price >= 0)
);

CREATE TABLE a6_countries (
    author_city VARCHAR2(50) PRIMARY KEY,
    author_country VARCHAR2(50) NOT NULL
);

CREATE TABLE a6_authors (
    author_name VARCHAR2(50) PRIMARY KEY,
    author_city VARCHAR2(50) REFERENCES a6_countries(author_city)
);

CREATE TABLE a6_authors_books (
    isbn VARCHAR2(13) REFERENCES a6_books(isbn),
    author_name VARCHAR2(50) REFERENCES a6_authors(author_name)
);

--Add data into tables
INSERT INTO a6_books
VALUES ('123ABC', 'How to book', 'Business', 20.23);
INSERT INTO a6_books
VALUES ('JKLXCD', 'Can you really ever book?', 'Business', 30.12);
INSERT INTO a6_books
VALUES ('AJKAJA', 'Wow hobbits!', 'Fantasy', 10.20);
INSERT INTO a6_books
VALUES ('ASDSAK', 'Geez its hobbits.', 'Fantasy', 4.50);
INSERT INTO a6_books
VALUES ('AJSHAK', 'Science Adventure', 'Sci-fi', 13.12);
INSERT INTO a6_books
VALUES ('OAISJD', 'It''s cooking', 'Cooking', 35.36);
INSERT INTO a6_books
VALUES ('ASDOA', 'The times and ideas of Ronald McRonald', 'Biography', 23);

INSERT INTO a6_countries
VALUES ('Montreal', 'Canada');
INSERT INTO a6_countries
VALUES ('Birmingham', 'England');
INSERT INTO a6_countries
VALUES ('Toronto', 'Canada');

INSERT INTO a6_authors
VALUES ('Reginald Auhtorson', 'Montreal');
INSERT INTO a6_authors
VALUES ('XYZ Tolkeen', 'Birmingham');
INSERT INTO a6_authors
VALUES ('B Ronalds', 'Birmingham');
INSERT INTO a6_authors
VALUES ('Janeet Paulton', 'Toronto');
INSERT INTO a6_authors
VALUES ('Vanti Raulson', 'Toronto');

INSERT INTO a6_authors_books
VALUES ('123ABC', 'Reginald Auhtorson');
INSERT INTO a6_authors_books
VALUES ('JKLXCD', 'Reginald Auhtorson');
INSERT INTO a6_authors_books
VALUES ('AJKAJA', 'XYZ Tolkeen');
INSERT INTO a6_authors_books
VALUES ('ASDSAK', 'XYZ Tolkeen');
INSERT INTO a6_authors_books
VALUES ('AJSHAK', 'B Ronalds');
INSERT INTO a6_authors_books
VALUES ('OAISJD', 'Janeet Paulton');
INSERT INTO a6_authors_books
VALUES ('ASDOA', 'Vanti Raulson');
INSERT INTO a6_authors_books
VALUES ('ASDOA', 'Janeet Paulton');

--Queries
SELECT COUNT(title) FROM a6_books
WHERE price > 15;

SELECT b.title, ab.author_name FROM a6_books b
INNER JOIN a6_authors_books ab USING (isbn)
WHERE category = 'Fantasy';

SELECT DISTINCT b.title FROM a6_books b
INNER JOIN a6_authors_books USING (isbn)
INNER JOIN a6_authors USING (author_name)
INNER JOIN a6_countries c USING (author_city)
WHERE c.author_country = 'Canada';

--Create view
CREATE OR REPLACE VIEW a6_view AS
    SELECT b.title, author_name, b.price, a.author_city FROM a6_books b
    INNER JOIN a6_authors_books USING (isbn)
    INNER JOIN a6_authors a USING (author_name)
    WHERE b.category = 'Business';
    
--Create indices
CREATE INDEX a6_book_index
ON a6_books (title, price);

CREATE INDEX a6_author_index
ON a6_authors (author_name, author_city);































