CREATE OR REPLACE PACKAGE book_store AS
    FUNCTION get_price_after_tax(book_isbn VARCHAR2) RETURN NUMBER;
END book_store;

/

CREATE OR REPLACE PACKAGE BODY book_store AS
    FUNCTION price_after_discount(book_isbn VARCHAR2)
        RETURN NUMBER IS
            final_price books.retail%TYPE; --matches the datatype of the variable to the retail variable from the books table
        BEGIN
            SELECT (b.retail - NVL(b.discount, 0)) INTO final_price FROM books b --NVL used to replace null discounts with 0
            WHERE b.isbn = book_isbn;
            
            RETURN final_price;
        END;
        
    FUNCTION get_price_after_tax(book_isbn VARCHAR2)
        RETURN NUMBER IS
            price_after_tax books.retail%TYPE;
        BEGIN
            price_after_tax := price_after_discount(book_isbn) * 1.15;
            
            RETURN price_after_tax;
        END;
END book_store;

/

BEGIN
    dbms_output.put_line(book_store.get_price_after_tax('4981341710'));
    dbms_output.put_line(book_store.get_price_after_tax('3957136468'));
END;