package jdbc;

import java.sql.*;

public class JustLeeServices {
    public static void main(String[] args){
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        String username = System.console().readLine("Username: ");
        String password = new String(System.console().readPassword("Password: "));
        Connection conn = getConnection(url, username, password);

        Book b = new Book("'0123456789'", "'BOOK TITLE'", "'28-SEP-23'", 2, 10, 10, 5, "'FITNESS'");

        try {
            addBook(b, conn);
            System.out.println(getBook("0123456789", conn));
            
            conn.close();
        }
        //If username or password is incorrect, addBook will give a NullPointerException
        catch (NullPointerException e){
            System.out.println("Username or password is incorrect.");
        }
        catch (SQLException e){
            System.out.println(e);
        }
    }

    public static Connection getConnection(String url, String username, String password){
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            System.out.println("Connection successful.\n");
            return conn;
        }
        catch(SQLException e){
            System.out.println(e);
            return null;
        }
    }

    public static Book getBook(String isbn, Connection conn){
        try {
            PreparedStatement s = conn.prepareStatement(
                "SELECT * FROM books b " +
                "WHERE b.isbn = " + isbn
                );

            ResultSet result = s.executeQuery();
            result.next();
            
            String title = result.getString("title");
            String pubDate = result.getString("pubdate");
            int pubID = result.getInt("pubid");
            double cost = result.getDouble("cost");
            double retail = result.getDouble("retail");
            double discount = result.getDouble("discount");
            String category = result.getString("category");

            Book b = new Book(isbn, title, pubDate, pubID, cost, retail, discount, category);

            if (!s.isClosed()){
                s.close();
            }

            return b;
        }
        catch (SQLException e){
            System.out.println(e);
            return null;
        }
    }

    public static void addBook(Book b, Connection conn){
        try {
            PreparedStatement s = conn.prepareStatement(
                "INSERT INTO books VALUES (" +
                "" + b.getIsbn() + ", " +
                "" + b.getTitle() + ", " +
                "" + b.getPubDate() + ", " +
                b.getPubID() + ", " +
                b.getCost() + ", " +
                b.getRetail() + ", " +
                b.getDiscount() + ", " +
                "" + b.getCategory() + ")"
            );

            //executeUpdate will run forever if the database is opened in SQLDeveloper while running this code
            int results = s.executeUpdate();
            System.out.println(results + " row added.\n");

            if (!s.isClosed()){
                s.close();
            }
        }
        catch (SQLException e){
            System.out.println(e);
        }
    }
}
