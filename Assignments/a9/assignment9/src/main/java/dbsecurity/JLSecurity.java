package dbsecurity;

import java.sql.*;
import java.util.Arrays;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class JLSecurity implements IJLSecurity {
    private Connection conn;

    public JLSecurity(Connection conn) {
        this.conn = conn;
    }

    @Override
    public void CreateUser(String user, String password) throws SQLException {
        try {
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
            byte[] salt = new byte[16];
            sr.nextBytes(salt);

            PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 1000, 512);
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] hash = skf.generateSecret(spec).getEncoded();

            IUser newUser = new User(user, salt, hash, 0);
            updateDB(newUser);
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean Login(String user, String password) throws SQLException {
        IUser userLogin = getUser(user);
        byte[] check_hash = null;

        try {
            PBEKeySpec check_spec = new PBEKeySpec(password.toCharArray(), userLogin.getSalt(), 1000, 512);
            SecretKeyFactory check_skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            check_hash = check_skf.generateSecret(check_spec).getEncoded();
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        
        return Arrays.equals(userLogin.getHash(), check_hash);
    }

    /**
     * Returns an IUser object from the database.
     * @param userName - Name of the user to return.
     * @return - The user matching the username.
     * @throws SQLException
     */
    private IUser getUser(String userName) throws SQLException {
        String sql = "SELECT * FROM jlusers WHERE userid = ?";

        PreparedStatement s = this.conn.prepareStatement(sql);
        s.setString(1, userName);

        ResultSet results = s.executeQuery();
        results.next(); // Since there is only 1 row that is returned, results.next() doesn't need to be in a while loop.
        IUser user = new User(results.getString(1), results.getBytes(2), results.getBytes(3), results.getLong(4));
        
        s.close();

        return user;
    }

    /**
     * Adds a new user to the JLUsers table.
     * @param user - User to add.
     * @throws SQLException
     */
    private void updateDB(IUser user) throws SQLException {
        String sql = "INSERT INTO jlusers VALUES (?,?,?,?)";

        PreparedStatement s = this.conn.prepareStatement(sql);
        s.setString(1, user.getUser());
        s.setBytes(2, user.getSalt());
        s.setBytes(3, user.getHash());
        s.setLong(4, user.getFailedLoginCount());

        int results = s.executeUpdate();
        System.out.println("\n" + results + " row added.");

        s.close();
    }

    /**
     * Gets the current failed login count of a user.
     * @param user - Name of the user to access.
     * @return - The number of failed logins from that user.
     * @throws SQLException
     */
    private int getFailedLoginCount(String user) throws SQLException {
        String select = "SELECT failedLoginCount FROM jlusers WHERE userid = ?";

        PreparedStatement s = this.conn.prepareStatement(select);
        s.setString(1, user);

        ResultSet results = s.executeQuery();
        results.next(); // Since there is only 1 row that is returned, results.next() doesn't need to be in a while loop.
        int failedLoginCount = results.getInt("failedLoginCount");

        s.close();

        return failedLoginCount;
    }

    /**
     * Increments the failed login count of a user by 1.
     * @param user - The user to modify.
     * @throws SQLException
     */
    public void addFailedLogin(String user) throws SQLException {
        String sql = "UPDATE jlusers SET failedLoginCount = ? WHERE userid = ?";

        PreparedStatement s = this.conn.prepareStatement(sql);
        s.setLong(1, getFailedLoginCount(user) + 1); // Gets the current failed login count of that user and adds 1 to it.
        s.setString(2, user);

        int results = s.executeUpdate();
        System.out.println("\n" + results + " row updated.");

        s.close();
    }

    /**
     * Closes the current connection.
     */
    public void closeConnection() {
        try {
            this.conn.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
