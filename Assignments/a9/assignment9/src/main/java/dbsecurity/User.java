package dbsecurity;

public class User implements IUser {
    private String user;
    private byte[] salt;
    private byte[] hash;
    private long failedLoginCount;

    public User(String user, byte[] salt, byte[] hash, long failedLoginCount) {
        this.user = user;
        this.salt = salt;
        this.hash = hash;
        this.failedLoginCount = failedLoginCount;
    }
    
    @Override
    public String getUser() {
        return this.user;
    }

    @Override
    public byte[] getSalt() {
        return this.salt;
    }

    @Override
    public byte[] getHash() {
        return this.hash;
    }

    @Override
    public long getFailedLoginCount() {
        return this.failedLoginCount;
    }

    @Override
    public void setFailedLoginCount(long count) {
        this.failedLoginCount = count;
    }
}
