package dbsecurity;

import java.sql.*;

public class App {
    public static void main(String[] args) {
        JLSecurity security;
        
        while (true) {
            try {
                String username = System.console().readLine("Enter your username: ");
                String password = new String(System.console().readPassword("Enter your password: "));

                security = new JLSecurity(getConnection(username, password));

                System.out.println("\nLogin successful. Welcome to the JustLee database.");
                
                break;
            }
            // Exception thrown if the connection is invalid (user entered in a wrong username or password).
            catch (SQLException e) {
                System.out.println("\nInvalid username or password.\n");
            }
        }
        
        while (true) {
            try {
                System.out.println("\n(1) Login an existing user - (2) Register a new user - (3) End session");
                String input = System.console().readLine("What would you like to do?: ");

                if (input.equals("1")) {
                    if (!loginUser(security)) { break; }
                }
                else if (input.equals("2")) {
                    registerUser(security);
                }
                else if (input.equals("3")) {
                    System.out.println("\nSuccessfully logged out.");
                    break;
                }
                else {
                    throw new IllegalArgumentException();
                }
            }
            // Exception thrown if the user doesn't pick from the options listed.
            catch (IllegalArgumentException e) {
                System.out.println("\nInvalid input. Please enter 1, 2 or 3.");
            }
        }

        security.closeConnection();
    }

    /**
     * Creates a new SQL connection with the username and password provided.
     * @param username - The user's username.
     * @param password - The user's password.
     * @return - An SQL connection.
     * @throws SQLException
     */
    private static Connection getConnection(String username, String password) throws SQLException {
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        return DriverManager.getConnection(url, username, password);
    }

    /**
     * Login used to access the JustLee database.
     * @param db - JLSecurity object to manage the logins.
     * @return - Validity of the login.
     */
    private static boolean loginUser(JLSecurity db) {
        int loginCount = 0; // Variable to keep track of the number of invalid logins.
        String username = "";

        while (true) {
            try {
                // If the user incorrectly enters their username/password 5 times,
                // they are locked out and cannot try again.
                if (loginCount == 5) {
                    System.out.println("\nYou have incorrectly entered your credentials 5 times. You are now locked out.");

                    try {
                        // Updates the failedLoginCount column of the JLUsers table in the database for a certain user.
                        // Potential bug: If the user is at 4 incorrect logins, the username whose failedLoginCount column gets
                        // updated in the database is the username that will be entered on the 5th try. If that username doesn't exist,
                        // then no row is updated.
                        db.addFailedLogin(username);
                    }
                    catch (SQLException e) {}
                    
                    return false;
                }

                username = System.console().readLine("\nEnter the username: ");
                String password = new String(System.console().readPassword("Enter the password: "));

                // If the Login function returns false, the hash of the user's password and the password
                // that was just entered in do not match.
                if (!db.Login(username, password)) {
                    throw new IllegalArgumentException();
                }

                System.out.println("\nSuccessfully logged in.");

                return true;
            }
            // If the username is invalid, an SQLException will be thrown.
            // If the password is invalid, an IllegalArgumentException will be thrown.
            // For security reasons, the program won't display exactly which one is invalid.
            catch (SQLException e) {
                System.out.println("\nThe username or password is invalid.");
                loginCount++;
            }
            catch (IllegalArgumentException e) {
                System.out.println("\nThe username or password is invalid.");
                loginCount++;
            }
        }
    }

    /**
     * Adds a new user to the database.
     * @param db - JLSecurity object to manage the registers.
     */
    private static void registerUser(JLSecurity db) {
        while (true) {
            try {
                String username = System.console().readLine("\nEnter a new username: ");
                String password = new String(System.console().readPassword("Enter a new password: "));

                db.CreateUser(username, password);
                System.out.println("User successfully registered.");

                break;
            }
            // If a user with the same username already exists, an exception is thrown.
            catch (SQLException e) {
                System.out.println("\nThat username already exists.");
            }
        }
    }
}
