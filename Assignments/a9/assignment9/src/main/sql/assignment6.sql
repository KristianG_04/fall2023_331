DROP TABLE jlusers;

CREATE TABLE jlusers (
    userid VARCHAR2(100) UNIQUE NOT NULL,
    salt RAW(16) NOT NULL,
    hash RAW(512) NOT NULL,
    failedLoginCount NUMBER(5) NOT NULL
);