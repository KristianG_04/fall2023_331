package coursedb;

import java.util.InputMismatchException;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        String username = System.console().readLine("Username: ");
        String password = new String(System.console().readPassword("Password: "));

        CourseListServices cls = new CourseListServices(username, password);

        try {
            String cNum = System.console().readLine("Enter the course number: ");
            String cName = System.console().readLine("Enter the course name: ");
            String desc = System.console().readLine("Enter the course description: ");
            System.out.print("Enter the class hours: ");
            int cHours = reader.nextInt();
            System.out.print("Enter the lab hours: ");
            int lHours = reader.nextInt();
            System.out.print("Enter the homework hours: ");
            int hHours = reader.nextInt();
            System.out.print("Enter the term number: ");
            int term = reader.nextInt();

            cls.addCourse(cNum, cName, desc, cHours, lHours, hHours, term);

            cls.closeConn();
        }
        catch(InputMismatchException e) { //If the user inputs anything other than an int for the class hours/term
            System.out.println("Not a valid input.");
            System.exit(1);
        }
    }
}