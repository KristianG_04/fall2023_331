package coursedb;

import java.sql.*;
import java.util.Map;

public class Term implements SQLData{
    private final String typeName = "TERM_TYP";

    private int term;
    private int semesterId;

    public Term() {

    }

    public Term(int term, int semesterId) {
        this.term = term;
        this.semesterId = semesterId;
    }

    public void addToDatabase(Connection conn) {
        try {
            Map map = conn.getTypeMap();
            conn.setTypeMap(map);
            map.put(this.typeName, Class.forName("coursedb.Term"));

            Term newTerm = new Term(this.term, this.semesterId);
            String sql = "{call add_term(?)}";

            CallableStatement s = conn.prepareCall(sql);
            s.setObject(1, newTerm);
            s.execute();
        }
        catch(SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
        catch(ClassNotFoundException c) {
            c.printStackTrace();
            System.exit(1);
        }
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) {
        try {
            setTerm(stream.readInt());
            setSemesterId(stream.readInt());
        }
        catch(SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    @Override
    public void writeSQL(SQLOutput stream) {
        try {
            stream.writeInt(getTerm());
            stream.writeInt(getSemesterId());
        }
        catch(SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    @Override
    public String getSQLTypeName() {
        return this.typeName;
    }

    public int getTerm() {
        return this.term;
    }

    public void setTerm(int term) {
        this.term = term;
    }

    public int getSemesterId() {
        return this.semesterId;
    }

    public void setSemesterId(int semesterId) {
        this.semesterId = semesterId;
    }
}
