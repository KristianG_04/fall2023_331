package coursedb;

import java.sql.*;
import java.util.Map;

public class Course implements SQLData {
    private final String typeName = "COURSE_TYP";

    private String course_number;
    private String course_name;
    private String description;
    private int class_hours;
    private int lab_hours;
    private int home_hours;
    private int term;

    public Course() {

    }

    public Course(String course_number, String course_name, String description, int class_hours, int lab_hours, int home_hours, int term) {
        this.course_number = course_number;
        this.course_name = course_name;
        this.description = description;
        this.class_hours = class_hours;
        this.lab_hours = lab_hours;
        this.home_hours = home_hours;
        this.term = term;
    }

    public void addToDatabase(Connection conn) {
        try {
            Map map = conn.getTypeMap();
            conn.setTypeMap(map);
            map.put(this.typeName, Class.forName("coursedb.Course"));

            Course newCourse = new Course(this.course_number, this.course_name, this.description, this.class_hours, this.lab_hours, this.home_hours, this.term);
            String sql = "{call add_course(?)}";

            CallableStatement s = conn.prepareCall(sql);
            s.setObject(1, newCourse);
            s.execute();
        }
        catch(SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
        catch(ClassNotFoundException c) {
            c.printStackTrace();
            System.exit(1);
        }
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) {
        try {
            setCourseNumber(stream.readString());
            setCourseName(stream.readString());
            setDescription(stream.readString());
            setClassHours(stream.readInt());
            setLabHours(stream.readInt());
            setHomeHours(stream.readInt());
            setTerm(stream.readInt());
        }
        catch(SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    @Override
    public void writeSQL(SQLOutput stream) {
        try {
            stream.writeString(getCourseNumber());
            stream.writeString(getCourseName());
            stream.writeString(getDescription());
            stream.writeInt(getClassHours());
            stream.writeInt(getLabHours());
            stream.writeInt(getHomeHours());
            stream.writeInt(getTerm());
        }
        catch(SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    @Override
    public String getSQLTypeName() {
        return this.typeName;
    }

    public String getCourseNumber() {
        return this.course_number;
    }

    public void setCourseNumber(String course_number) {
        this.course_number = course_number;
    }

    public String getCourseName() {
        return this.course_name;
    }

    public void setCourseName(String course_name) {
        this.course_name = course_name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getClassHours() {
        return this.class_hours;
    }

    public void setClassHours(int class_hours) {
        this.class_hours = class_hours;
    }

    public int getLabHours() {
        return this.lab_hours;
    }

    public void setLabHours(int lab_hours) {
        this.lab_hours = lab_hours;
    }

    public int getHomeHours() {
        return this.home_hours;
    }

    public void setHomeHours(int home_hours) {
        this.home_hours = home_hours;
    }

    public int getTerm() {
        return this.term;
    }

    public void setTerm(int term) {
        this.term = term;
    }
}
