package coursedb;

import java.sql.*;

public class CourseListServices {
    private Connection conn;

    public CourseListServices(String user, String pass) {
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";

        try {
            this.conn = DriverManager.getConnection(url, user, pass);
        }
        catch(SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public void closeConn() {
        try {
            this.conn.close();
        }
        catch(SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public void addCourse(String course_number, String course_name, String description, int class_hours, int lab_hours, int home_hours, int term) {
        Course c = new Course(course_number, course_name, description, class_hours, lab_hours, home_hours, term);
        c.addToDatabase(this.conn);
    }
}
