--Drop existing tables
DROP TABLE seasons CASCADE CONSTRAINTS;
DROP TABLE terms CASCADE CONSTRAINTS;
DROP TABLE courses CASCADE CONSTRAINTS;

--Create tables
CREATE TABLE seasons (
    semester_id NUMBER(1) PRIMARY KEY,
    season VARCHAR2(15) NOT NULL
);

CREATE TABLE terms (
    term NUMBER(1) PRIMARY KEY,
    semester_id NUMBER(1) REFERENCES seasons(semester_id) NOT NULL
);

CREATE TABLE courses (
    course_number VARCHAR2(10) PRIMARY KEY,
    course_name VARCHAR2(50) NOT NULL,
    description VARCHAR2(500) NOT NULL,
    class_hours NUMBER(2) NOT NULL,
    lab_hours NUMBER(2) NOT NULL,
    home_hours NUMBER(2) NOT NULL,
    term NUMBER(1) REFERENCES terms(term) NOT NULL
);

--Add data
INSERT INTO seasons
VALUES (1, 'FALL');
INSERT INTO seasons
VALUES (2, 'WINTER');
INSERT INTO seasons
VALUES (3, 'SUMMER');

INSERT INTO terms
VALUES (1, 1);
INSERT INTO terms
VALUES (2, 2);
INSERT INTO terms
VALUES (3, 1);
INSERT INTO terms
VALUES (4, 2);
INSERT INTO terms
VALUES (5, 1);
INSERT INTO terms
VALUES (6, 2);

INSERT INTO courses
VALUES ('420-110-DW', 'Programming I', 'The course will introduce the student to the basic building blocks (sequential,
selection and repetitive control structures) and modules (methods and classes)
used to write a program.', 3, 3, 3, 1);
INSERT INTO courses
VALUES ('420-210-DW', 'Programming II', 'The course will introduce the student to basic object-oriented methodology in
order to design, implement, use and modify classes, to write programs in the
Java language that perform interactive processing, array and string processing,
and data validation.', 3, 3, 3, 2);

--Create types for each Java class
CREATE OR REPLACE TYPE course_typ AS OBJECT (
    course_number VARCHAR2(10),
    course_name VARCHAR2(50),
    description VARCHAR2(500),
    class_hours NUMBER(2),
    lab_hours NUMBER(2),
    home_hours NUMBER(2),
    term NUMBER(1)
);

/

CREATE OR REPLACE TYPE term_typ AS OBJECT (
    term NUMBER(1),
    semester_id NUMBER(1)
);

/

CREATE OR REPLACE TYPE season_typ AS OBJECT (
    semester_id NUMBER(1),
    season VARCHAR2(15)
);

/

--Add procedures
CREATE OR REPLACE PROCEDURE add_course (vcourse IN course_typ) AS
    BEGIN
        INSERT INTO courses
        VALUES (vcourse.course_number, vcourse.course_name, vcourse.description, vcourse.class_hours, vcourse.lab_hours, vcourse.home_hours, vcourse.term);
    END;
    
/

CREATE OR REPLACE PROCEDURE add_term (vterm IN term_typ) AS
    BEGIN
        INSERT INTO terms
        VALUES (vterm.term, vterm.semester_id);
    END;
    
/
    
CREATE OR REPLACE PROCEDURE add_season (vseason IN season_typ) AS
    BEGIN
        INSERT INTO seasons
        VALUES (vseason.semester_id, vseason.season);
    END;

/











