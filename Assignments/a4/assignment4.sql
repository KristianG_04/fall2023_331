DROP PACKAGE book_store;

CREATE OR REPLACE PACKAGE book_store AS
    FUNCTION get_price_after_tax(book_isbn VARCHAR2) RETURN NUMBER;
    --Assignment 3
    TYPE customer_id_array_type IS VARRAY(100) OF NUMBER;
    PROCEDURE show_purchases;
    --Assignment 4
    PROCEDURE rename_category(category_name IN VARCHAR2, to_change IN VARCHAR2);
    PROCEDURE add_publisher(pub_id IN NUMBER, pub_name IN VARCHAR2, pub_contact IN VARCHAR2, pub_phone IN VARCHAR2);
    category_not_found EXCEPTION;
    publisher_already_exists EXCEPTION;
END book_store;

/

CREATE OR REPLACE PACKAGE BODY book_store AS
    FUNCTION price_after_discount(book_isbn VARCHAR2)
        RETURN NUMBER IS
            final_price books.retail%TYPE; --matches the datatype of the variable to the retail variable from the books table
        BEGIN
            SELECT (b.retail - NVL(b.discount, 0)) INTO final_price FROM books b --NVL used to replace null discounts with 0
            WHERE b.isbn = book_isbn;
            
            RETURN final_price;
        END;
        
    FUNCTION get_price_after_tax(book_isbn VARCHAR2)
        RETURN NUMBER IS
            price_after_tax books.retail%TYPE;
        BEGIN
            price_after_tax := price_after_discount(book_isbn) * 1.15;
            
            RETURN price_after_tax;
        END;
        
    --Assignment 3
    FUNCTION book_purchasers(book_isbn VARCHAR2)
        RETURN customer_id_array_type IS
            id_array customer_id_array_type;
        BEGIN
            SELECT customer# BULK COLLECT INTO id_array FROM customers c
            INNER JOIN orders USING (customer#)
            INNER JOIN orderitems oi USING (order#)
            WHERE oi.isbn = book_isbn;
            
            RETURN id_array;
        END;
        
    PROCEDURE show_purchases AS
        list_of_customers VARCHAR2(200);
        c_firstname VARCHAR2(50);
        c_lastname VARCHAR2(50);
        current_id NUMBER;
        BEGIN
            FOR row IN (SELECT * FROM books) LOOP
                FOR i IN 1 .. book_purchasers(row.isbn).COUNT LOOP
                    current_id := book_purchasers(row.isbn)(i);
                
                    SELECT firstname INTO c_firstname FROM customers
                    WHERE customer# = current_id; --Gets the first name of the customer with the current id
                    
                    SELECT lastname INTO c_lastname FROM customers
                    WHERE customer# = current_id; --Gets the last name of the customer with the current id
                    
                    list_of_customers := list_of_customers || c_firstname || ' ' || c_lastname;
                    
                    --If statement to prevent an extra comma from being added at the end of the list of names
                    IF i != book_purchasers(row.isbn).COUNT THEN
                        list_of_customers := list_of_customers || ', ';
                    END IF;    
                END LOOP;
                
                dbms_output.put_line(row.isbn || ': ' || row.title || ': ' || list_of_customers);
                
                list_of_customers := ''; --Empties the variable for the next iteration of the loop
            END LOOP;
        END;
        
    --Assignment 4
    PROCEDURE rename_category(category_name IN VARCHAR2, to_change IN VARCHAR2) AS
        BEGIN
            UPDATE books SET category = to_change
            WHERE UPPER(category) = UPPER(category_name); --UPPER function used here to ignore upper/lower case letters
            
        IF SQL%ROWCOUNT = 0 THEN --Checks if the number of rows updated is 0
            RAISE category_not_found;
        END IF;
        
        EXCEPTION
            WHEN category_not_found THEN
                dbms_output.put_line('This category does not exist.');
        END;
        
    PROCEDURE add_publisher(pub_id IN NUMBER, pub_name IN VARCHAR2, pub_contact IN VARCHAR2, pub_phone IN VARCHAR2) AS
        BEGIN
            FOR row IN (SELECT * FROM publisher) LOOP
                IF UPPER(row.name) = UPPER(pub_name) THEN --Checks if the name inputted into the function matches that of an existing publisher in the table
                    RAISE publisher_already_exists;
                END IF;
            END LOOP;
            
            INSERT INTO publisher
            VALUES (pub_id, pub_name, pub_contact, pub_phone);
            
        EXCEPTION
            WHEN dup_val_on_index THEN
                dbms_output.put_line('Two publishers cannot have the same id.');
            WHEN publisher_already_exists THEN
                dbms_output.put_line('This publisher already exists.');
        END;
END book_store;

/

BEGIN
    dbms_output.put_line(book_store.get_price_after_tax('4981341710'));
    dbms_output.put_line(book_store.get_price_after_tax('3957136468'));
END;

/

--Assignment 3
BEGIN
    book_store.show_purchases;
END;

/

--Assignment 4
BEGIN
    book_store.rename_category('COMPUTER', 'COMP. SCI.');
    book_store.rename_category('TEACHING', 'EDUCATION');
    
    book_store.add_publisher(6, 'DAWSON PRINTING', 'JOHN SMITH', '111-555-2233');
    book_store.add_publisher(7, 'PUBLISH OUR WAY', 'JANE TOMLIN', '010-410-0010');
END;

/