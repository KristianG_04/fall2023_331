--Part 1:
CREATE TABLE home (
    hid CHAR(4) PRIMARY KEY,
    address VARCHAR2(200) NOT NULL
);

CREATE TABLE occupation (
    oid CHAR(4) PRIMARY KEY,
    type VARCHAR2(20) NOT NULL,
    salary NUMBER(10,2)
);

CREATE TABLE person (
    pid CHAR(4) PRIMARY KEY,
    firstname VARCHAR2(20) NOT NULL,
    lastname VARCHAR2(20) NOT NULL,
    father_id CHAR(4) REFERENCES person(pid),
    mother_id CHAR(4) REFERENCES person(pid),
    hid CHAR(4) REFERENCES home(hid),
    oid CHAR(4) REFERENCES occupation(oid)
);

--Part 2:
INSERT INTO home
VALUES ('H001', '123 Easy St.');
INSERT INTO home
VALUES ('H002', '56 Fake Ln.');

INSERT INTO occupation
VALUES ('O000', 'N/A', 0);
INSERT INTO occupation
VALUES ('O001', 'Student', 0);
INSERT INTO occupation
VALUES ('O002', 'Doctor', 100000);
INSERT INTO occupation
VALUES ('O003', 'Professor', 80000);

INSERT INTO person (pid, firstname, lastname, hid, oid)
VALUES ('P001', 'Zachary', 'Aberny', 'H002', 'O000');
INSERT INTO person (pid, firstname, lastname, hid, oid)
VALUES ('P002', 'Yanni', 'Aberny', 'H002', 'O000');
INSERT INTO person
VALUES ('P003', 'Alice', 'Aberny', 'P001', 'P002', 'H001', 'O002');
INSERT INTO person (pid, firstname, lastname, hid, oid)
VALUES ('P004', 'Bob', 'Bortelson', 'H001', 'O003');
INSERT INTO person
VALUES ('P005', 'Carl', 'Aberny-Bortelson', 'P004', 'P003', 'H001', 'O001');
INSERT INTO person
VALUES ('P006', 'Denise', 'Aberny-Bortelson', 'P004', 'P003', 'H001', 'O001');

SELECT gp.firstname FROM person gp
JOIN person p ON gp.pid = p.mother_id OR gp.pid = p.father_id
JOIN person c ON p.pid = c.mother_id OR p.pid = c.father_id
WHERE c.firstname = 'Denise';

--Part 3:
SELECT h.address, COUNT(p.pid) FROM home h
INNER JOIN person p USING (hid)
GROUP BY h.address;

SELECT gp.firstname, gp.lastname FROM person gp
JOIN person p ON gp.pid = p.father_id
JOIN person c ON p.pid = c.mother_id OR p.pid = c.father_id
WHERE c.firstname = 'Denise';

SELECT p.firstname, p.lastname FROM person p
INNER JOIN home h USING (hid)
INNER JOIN occupation o USING (oid)
WHERE (p.mother_id IS NOT NULL OR p.father_id IS NOT NULL) AND o.type = 'Student';

SELECT h.address, (SUM(o.salary)) AS "INCOME" FROM home h
INNER JOIN person USING (hid)
INNER JOIN occupation o USING (oid)
GROUP BY h.address
ORDER BY "INCOME" DESC
FETCH FIRST ROW ONLY;