CREATE OR REPLACE TYPE publisher_typ AS OBJECT (
    pubID NUMBER(2, 0),
    name VARCHAR2(23),
    contact VARCHAR2(15),
    phone VARCHAR2(12)
);

/

CREATE OR REPLACE PROCEDURE add_author (vpublisher IN publisher_typ) AS
    BEGIN
        INSERT INTO publisher
        VALUES (vpublisher.pubID, vpublisher.name, vpublisher.contact, vpublisher.phone);
    END;
    
/

CREATE OR REPLACE FUNCTION get_publisher (vpubID NUMBER) RETURN publisher_typ
    AS
        vpublisher publisher_typ;
    BEGIN
        SELECT pubID, name, contact, phone
        INTO vpublisher.pubID, vpublisher.name, vpublisher.contact, vpublisher.phone
        FROM publisher
        WHERE pubID = vpubID;
        
        RETURN vpublisher;
    END;
    
/

DECLARE
    pub publisher_typ;
BEGIN
    pub := get_publisher(2);
    --dbms_output.put_line(pub.pubID);
END;