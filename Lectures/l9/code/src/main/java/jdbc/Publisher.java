package jdbc;

public class Publisher {
    private final String TYPENAME = "PUBLISHER_TYP";
    private int pubID;
    private String name;
    private String contact;
    private String phone;

    public Publisher(int pubID, String name, String contact, String phone) {
        this.pubID = pubID;
        this.name = name;
        this.contact = contact;
        this.phone = phone;
    }

    public int getPubID() {
        return this.pubID;
    }

    public String getName() {
        return this.name;
    }

    public String getContact() {
        return this.contact;
    }

    public String getPhone() {
        return this.phone;
    }

    @Override
    public String toString() {
        return this.pubID + ", " + this.name + ", " + this.contact + ", " + this.phone;
    }
}
