package jdbc;

public class Book {
    private String isbn;
    private String title;
    private String pubDate;
    private Publisher publisher;
    private double cost;
    private double retail;
    private double discount;
    private String category;

    public Book(String isbn, String title, String pubDate, Publisher publisher, double cost, double retail, double discount, String category){
        this.isbn = isbn;
        this.title = title;
        this.pubDate = pubDate;
        this.publisher = publisher;
        this.cost = cost;
        this.retail = retail;
        this.discount = discount;
        this.category = category;
    }

    public String getIsbn(){
        return this.isbn;
    }
    
    public String getTitle(){
        return this.title;
    }

    public String getPubDate(){
        return this.pubDate;
    }

    public Publisher getPublisher(){
        return this.publisher;
    }

    public double getCost(){
        return this.cost;
    }

    public double getRetail(){
        return this.retail;
    }

    public double getDiscount(){
        return this.discount;
    }

    public String getCategory(){
        return this.category;
    }

    public String toString(){
        return this.isbn + ", " + this.title + ", " + this.pubDate + ", " + this.publisher.getPubID() + ", " + this.cost + ", " + this.retail + ", " + this.discount + ", " + this.category;
    }
}
