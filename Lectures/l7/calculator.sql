CREATE OR REPLACE PACKAGE calculator_pkg AS
    FUNCTION add(num1 NUMBER, num2 NUMBER) RETURN NUMBER;
    FUNCTION subtract(num1 NUMBER, num2 NUMBER) RETURN NUMBER;
    FUNCTION multiply(num1 NUMBER, num2 NUMBER) RETURN NUMBER;
    FUNCTION divide(num1 NUMBER, num2 NUMBER) RETURN NUMBER;
END calculator_pkg;

/

CREATE OR REPLACE PACKAGE BODY calculator_pkg AS
    FUNCTION add(num1 NUMBER, num2 NUMBER) RETURN NUMBER IS
        BEGIN
            RETURN num1 + num2;
        END;
    FUNCTION subtract(num1 NUMBER, num2 NUMBER) RETURN NUMBER IS
        BEGIN
            RETURN num1 - num2;
        END;
    FUNCTION multiply(num1 NUMBER, num2 NUMBER) RETURN NUMBER IS
        BEGIN
            RETURN num1 * num2;
        END;
    FUNCTION divide(num1 NUMBER, num2 NUMBER) RETURN NUMBER IS
        BEGIN
            IF num2 = 0 THEN
                RAISE zero_divide;
            END IF;
            
            RETURN num1 / num2;
        END;
END calculator_pkg;

/

BEGIN
    dbms_output.put_line(calculator_pkg.add(2, 6));
    dbms_output.put_line(calculator_pkg.subtract(9, 8));
    dbms_output.put_line(calculator_pkg.multiply(6, 4));
    dbms_output.put_line(calculator_pkg.divide(12, 3));
    dbms_output.put_line(calculator_pkg.divide(10, 0));
    
EXCEPTION
    WHEN zero_divide THEN
        dbms_output.put_line('Cannot divide by 0.');
END;