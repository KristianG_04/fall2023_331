CREATE OR REPLACE PACKAGE testpkg AS
    FUNCTION pkgfunc (testnum NUMBER) RETURN NUMBER;
END testpkg;

CREATE OR REPLACE PACKAGE BODY testpkg AS
    FUNCTION pkgfunc (testnum NUMBER) RETURN NUMBER IS
    BEGIN
        dbms_output.put_line(testnum);
    END;
END testpkg;
