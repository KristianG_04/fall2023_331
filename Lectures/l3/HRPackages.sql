CREATE OR REPLACE PACKAGE hr_package AS
    FUNCTION get_dept_id (hrdept_name VARCHAR2) RETURN NUMBER;
    FUNCTION get_city (hrdept_name VARCHAR2) RETURN VARCHAR2;
END hr_package;

/

CREATE OR REPLACE PACKAGE BODY hr_package AS
    FUNCTION get_dept_id (hrdept_name VARCHAR2)
        RETURN NUMBER IS
            hrdept_id NUMBER;
        BEGIN
            SELECT d.department_id INTO hrdept_id FROM hr.departments d
            WHERE d.department_name = hrdept_name;
            
            RETURN hrdept_id;
        END;
    FUNCTION get_city_name (newlocation_id NUMBER)
        RETURN VARCHAR2 IS
            city_name hr.locations.city%TYPE;
        BEGIN
            SELECT l.city INTO city_name FROM hr.locations l
            WHERE l.location_id = newlocation_id;
            
            RETURN city_name; 
        END;
    FUNCTION get_city (hrdept_name VARCHAR2)
        RETURN VARCHAR2 IS
            city_name hr.locations.city%TYPE;
        BEGIN
            SELECT l.city INTO city_name FROM hr.locations l
            INNER JOIN hr.departments d USING (location_id)
            WHERE d.department_name = hrdept_name;
            
            RETURN city_name;
        END;
END hr_package;

/

BEGIN
    --dbms_output.put_line(hr_package.get_dept_id('Marketing'));
    --dbms_output.put_line(hr_package.get_city_name(1000));
    --dbms_output.put_line(hr_package.get_city('Marketing'));
END;