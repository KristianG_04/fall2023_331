CREATE OR REPLACE PACKAGE hr_package AS
    FUNCTION get_dept_id (hrdept_name VARCHAR2) RETURN NUMBER;
    FUNCTION get_city (hrdept_name VARCHAR2) RETURN VARCHAR2;
    TYPE dept_id_array_type IS VARRAY(30) OF NUMBER;
    FUNCTION print_department_names RETURN NUMBER;
END hr_package;

/

CREATE OR REPLACE PACKAGE BODY hr_package AS
    FUNCTION get_dept_id (hrdept_name VARCHAR2)
        RETURN NUMBER IS
            hrdept_id NUMBER;
        BEGIN
            SELECT d.department_id INTO hrdept_id FROM hr.departments d
            WHERE d.department_name = hrdept_name;
            
            RETURN hrdept_id;
        END;
        
    FUNCTION get_city_name (newlocation_id NUMBER)
        RETURN VARCHAR2 IS
            city_name hr.locations.city%TYPE;
        BEGIN
            SELECT l.city INTO city_name FROM hr.locations l
            WHERE l.location_id = newlocation_id;
            
            RETURN city_name; 
        END;
        
    FUNCTION get_city (hrdept_name VARCHAR2)
        RETURN VARCHAR2 IS
            city_name hr.locations.city%TYPE;
        BEGIN
            SELECT l.city INTO city_name FROM hr.locations l
            INNER JOIN hr.departments d USING (location_id)
            WHERE d.department_name = hrdept_name;
            
            RETURN city_name;
        END;
        
    FUNCTION print_department_names RETURN NUMBER IS
        BEGIN
            FOR row IN (SELECT * FROM hrdepartments) LOOP
                dbms_output.put_line(row.department_id || ' ' || row.department_name);
            END LOOP;
            
            RETURN 0;
        END;
END hr_package;

/

BEGIN
    dbms_output.put_line(hr_package.get_dept_id('Marketing'));
    --dbms_output.put_line(hr_package.get_city_name(1000));
    --dbms_output.put_line(hr_package.get_city('Marketing'));
END;

/

CREATE OR REPLACE PROCEDURE add_dept_manager(dept_name IN VARCHAR2, newManager_id IN NUMBER, dept_id IN NUMBER) AS
BEGIN
    UPDATE hrdepartments SET manager_id = newManager_id
    WHERE department_id = dept_id AND department_name = dept_name;
    
    dbms_output.put_line(dept_id || ' was changed.');
END;

/

BEGIN
    add_dept_manager('Treasury', 200, 120);
END;

/

SELECT e.first_name, e.last_name FROM hr.employees e
WHERE e.department_id = hr_package.get_dept_id('Marketing');

/

BEGIN
    DECLARE
        dept_id_array hr_package.dept_id_array_type;
    BEGIN
        SELECT department_id BULK COLLECT INTO dept_id_array FROM hrdepartments;
        
        FOR i IN 1 .. dept_id_array.COUNT LOOP
            dbms_output.put_line(dept_id_array(i));
        END LOOP;
    END;
END;

/

BEGIN        
    dbms_output.put_line(hr_package.print_department_names);
END;