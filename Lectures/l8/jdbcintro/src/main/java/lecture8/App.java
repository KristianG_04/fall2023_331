package lecture8;

import java.sql.*;

public class App {
    public static void main(String[] args){
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        String username = System.console().readLine("Username: ");
        String password = new String(System.console().readPassword("Password: "));

        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            PreparedStatement s = conn.prepareStatement("select * from books");
            ResultSet result = s.executeQuery();

            while (result.next()){
                System.out.println(result.getString("title"));
            }

            if (!s.isClosed()){
                System.out.println("Closing statement.");
                s.close();
            }
            if (!conn.isClosed()){
                System.out.println("Closing connection.");
                conn.close();
            }
        }
        catch (SQLException e) {
            System.out.println("Error.");
        }
    }
}
